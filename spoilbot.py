import random
import re
import sqlite3

from disco.api import http
from disco.bot.plugin import Plugin
from disco.types import message, user


class SpoilBotPlugin(Plugin):
    db = None
    spoiler_re = None
    delete_re = None
    dms = {}

    def load(self, ctx):
        super(SpoilBotPlugin, self).load(ctx)
        spoiler_tag = self.config['spoiler_tag']
        self.spoiler_re = re.compile(r'\[{0}\]((?:.(?!\[s(?:poil(?:er)?)?\]))+)\[/{0}\]'.format(spoiler_tag))
        self.delete_re = re.compile(r'^!delete(?: (\d+))?$')
        self.db = sqlite3.connect('spoils.db')
        cursor = self.db.cursor()
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS spoils(
           id INTEGER PRIMARY KEY UNIQUE,
           channel_id INTEGER,
           user_id INTEGER,
           content TEXT,
           timestamp TEXT
        )
        """)
        self.db.commit()

    @Plugin.listen('MessageCreate')
    def on_message_create(self, event):
        if event.author.bot:
            return
        if event.message.channel.is_guild:
            self.on_guild_message_create(event)
        else:
            self.on_private_message_create(event)

    def on_guild_message_create(self, event):
        msg = event.message
        censored = self.spoiler_re.sub(self.config['spoiler_substitute'], msg.content)
        if censored != msg.content:
            msg.delete()
            embed = message.MessageEmbed()
            embed.description = censored
            embed.set_author(name=msg.author.username, icon_url=msg.author.avatar_url)
            embed.set_footer(text=self.config['footer_text'])
            mentions = []
            if msg.mention_everyone:
                mentions.append("@everyone")
            for role_id in msg.mention_roles:
                mentions.append("<@&{}>".format(role_id))
            for user_id in msg.mentions:
                mentions.append("<@!{}>".format(user_id))
            censored_msg = event.reply(" ".join(mentions), embed=embed)
            uncensored = self.spoiler_re.sub(r'\1', msg.content)
            cursor = self.db.cursor()
            cursor.execute(
                "INSERT INTO spoils VALUES (?, ?, ?, ?, ?)",
                (censored_msg.id, msg.channel.id, msg.author.id, uncensored, msg.timestamp.isoformat())
            )
            self.db.commit()
            censored_msg.add_reaction(self.config['reaction_emoji'])
        elif self.state.me.id in msg.mentions:
            event.reply(self.config['mention_response'].format(msg.author.mention))

    def on_private_message_create(self, event):
        match = self.delete_re.match(event.message.content)
        if not match:
            if event.author is not self.state.me:
                self.send_help(event.author)
            return
        if match.group(1) is None:
            messages = event.message.channel.messages
            messages.before = event.message.id
            messages.chunk_size = 1
            messages.fill()
            last_message = messages.next()
            try:
                match = self.delete_re.match(last_message.embeds[0].footer.text)
                delete_message_id = match.group(1)
            except (IndexError, AttributeError):
                delete_message_id = None
        else:
            delete_message_id = match.group(1)
        if delete_message_id is None:
            event.reply(self.config['pm_messages']['not_found'])
        else:
            cursor = self.db.cursor()
            cursor.execute("SELECT user_id, channel_id FROM spoils WHERE id=?", (delete_message_id,))
            fetch = cursor.fetchone()
            if fetch is None:
                event.reply(self.config['pm_messages']['already_deleted'])
            elif fetch[0] != event.author.id:
                event.reply(self.config['pm_messages']['wrong_owner'])
            else:
                self.client.api.channels_messages_delete(fetch[1], delete_message_id)
                event.reply(self.config['pm_messages']['successful_delete'])

    @Plugin.listen('MessageDelete')
    def on_message_delete(self, event):
        if event.channel.is_guild is False:
            return
        cursor = self.db.cursor()
        cursor.execute('DELETE FROM spoils WHERE id=?', (event.id,))
        self.db.commit()

    @Plugin.listen('MessageReactionAdd')
    def on_message_reaction_add(self, event):
        if event.emoji.to_string() != self.config['reaction_emoji']:
            return
        if event.user_id == self.client.state.me.id or not event.channel.is_guild:
            return
        cursor = self.db.cursor()
        cursor.execute("SELECT id, user_id, content, timestamp FROM spoils WHERE id = ?", (event.message_id,))
        fetch = cursor.fetchone()
        cursor.close()
        if not fetch:
            return
        try:
            author = self.client.state.users[fetch[1]]
        except KeyError:
            try:
                r = self.client.api.http(http.Routes.USERS_GET, dict(user=fetch[1]))
                author = user.User.create(self.client, r.json())
            except http.APIException as e:
                if e.code != 10013:
                    raise e
                author = None
        embed = message.MessageEmbed()
        embed.description = fetch[2]
        if fetch[1] == event.user_id:
            embed.set_footer(text="!delete {}".format(fetch[0]))
        embed.timestamp = fetch[3]
        if author:
            embed.set_author(name=author.username, icon_url=author.avatar_url)
        else:
            fake_user = user.User()
            fake_user.discriminator = random.randint(0, 9999)
            embed.set_author(name=self.config['unknown_user'], icon_url=fake_user.get_avatar_url())
        dm_channel = self.get_user_dm_channel(event.user_id)
        try:
            dm_channel.send_message(embed=embed)
        except http.APIException as e:
            if e.code != 50007:
                raise e
            error_message = self.config['no_pm_response'].format("<@{}>".format(event.user_id))
            self.send_destructive_message(event.channel_id, error_message)
            self.client.api.channels_messages_reactions_delete(
                event.channel_id, event.message_id, event.emoji.to_string(), event.user_id
            )

    def send_destructive_message(self, channel_id, message):
        message_object = self.client.api.channels_messages_create(channel_id, message)

        def destroy_message():
            self.client.api.channels_messages_delete(channel_id, message_object.id)
        self.spawn_later(self.config['no_pm_timer'], destroy_message)

    def get_user_dm_channel(self, user_id):
        try:
            dm_channel_id = self.dms[user_id]
            dm_channel = self.state.dms[dm_channel_id]
        except KeyError:
            dm_channel = self.client.api.users_me_dms_create(user_id)
            self.dms[user_id] = dm_channel.id
        return dm_channel

    def send_help(self, discord_user):
        embed = message.MessageEmbed()
        embed.title = "SpoilBot Help"
        embed.url = "https://framagit.org/kyriog/spoilbot"
        embed.set_footer(text="SpoilBot is an Open Source Project made by Kyriog")
        for field in self.config['help_messages']:
            embed.add_field(name=field['name'], value=field['value'])
        dm_channel = self.get_user_dm_channel(discord_user.id)
        dm_channel.send_message(embed=embed)
